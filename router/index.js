import express from "express";
import json from 'body-parser';

export const router = express.Router();
import alumnosDb from "../models/alumnos.js";
export default {router};
//declarar primer ruta por omision

router.get('/',(req,res)=>{

    res.render('index',{titulo:"Mis Practicas",nombre:"Jose Yair Lizarraga Acosta"})
});

router.get('/tabla',(req,res)=>{
//parametros
const params = {
    numero: req.query.numero
}
res.render('tabla',params);
});
2


router.post('/tabla',(req,res)=>{
    //parametros
    const params = {
        numero: req.body.numero
    }
    res.render('tabla',params);
});
    
router.get('/cotizacion', (req, res) => {

    const params = {
        valor: req.query.valor,
        pinicial: req.query.pinicial,
        plazos: req.query.plazos,
        valorFinal: req.query.valorFinal,
        cuotaMensual: req.query.cuotaMensual,
    };

     res.render('cotizacion', params);
});

router.post('/cotizacion', (req, res) => {

    const params = {
        valor: req.body.valor,
        pinicial: req.body.pinicial,
        plazos: req.body.plazos,
        valorFinal: req.body.valorFinal,
        cuotaMensual: req.body.cuotaMensual,
    };

     res.render('cotizacion', params);
});



//cambios 



let rows;
router.get('/alumnos', async(req,res)=>{

    rows = await alumnosDb.mostrarTodos();
    res.render('alumnos', {reg:rows});
});

let params;
router.post('/alumnos', async(req,res)=>{
    try{
        const params={
            matricula:req.body.matricula,
            nombre:req.body.nombre,
            domicilio:req.body.domicilio,
            sexo:req.body.sexo,
            especialidad:req.body.especialidad
        }
        await  alumnosDb.insertar(params);
    }catch(error){
        console.error(error);
        res.status(400).send('sucedio un error:' + error);
    }
    res.redirect('/alumnos');
    
   /* rows = await alumnosDb.mostrarTodos();
    res.render('alumnos',{ reg: rows });*/
});